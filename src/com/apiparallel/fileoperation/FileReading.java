package com.apiparallel.fileoperation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.apiparallel.filedetails.FileDetails;
import com.apiparallel.main.ParallelInterfaceCall;
import com.google.gson.Gson;

public class FileReading extends PrintStream{
	 private static final PrintStream originalSystemOut = System.out;
	    private static FileReading systemOutToLogger;
		
		private String packageOrClassToLog;
		final static Logger logger = Logger.getLogger(ParallelInterfaceCall.class.getName());
	    private FileReading(PrintStream original, String packageOrClassToLog) {
	        super(original);
	        this.packageOrClassToLog = packageOrClassToLog;
	    }
		//public static void fileRead()
		public static List<FileDetails> list=new ArrayList<FileDetails>(); 
		static FileDetails fd = new FileDetails();
		
		
		
		public static FileDetails fileRead() throws IOException
		{  
			
		
			try  
			{  
				File file=new File("C:\\Users\\jayal\\Request.json");    //creates a new file instance  
				FileReader fr=new FileReader(file);   //reads the file  
				BufferedReader br=new BufferedReader(fr);  //creates a buffering character input stream  
				StringBuffer sb=new StringBuffer();    //constructs a string buffer with no characters 
				
				String line;
			
				
				
				
				
				//System.out.println("POST Response Code : " + responseCode);
				//System.out.println("POST Response message : " + postConnection.getResponseMessage());
				while((line=br.readLine())!=null)  
				{  
					fd=null;
					sb.append(line);      //appends line to string buffer  
					sb.append("\n");     //line feed 
					
					
					
					Gson gson = new Gson();
					String json = line;
				
					fd = gson.fromJson(json, FileDetails.class);
				
				   // System.out.println("obj.name:" + obj.getName() + "\tobj.job:"+ obj.getJob());
				    list.add(fd);
				    //logger.info("Json string converted to json object.");
				    //postConnection.setDoOutput(true);
				    
					

				    
				
			}  
				fr.close();    //closes the stream and release the resources  
				//System.out.println("Contents of File: ");  
				//System.out.println(sb.toString());   //returns a string that textually represents the object
				logger.info("File Read operation completed successfully!");
			
				//logger.info("API POST Executed Successfully!");
			}  
			catch (IOException exception)  
			{  
				logger.info("File Read operation Failed!");
			}
			
			return fd;
		}
		
		public static void apicall(List<FileDetails> list2)throws IOException
		{
			
			URL urlobj = new URL("https://reqres.in/api/users");
			HttpURLConnection postConnection = (HttpURLConnection) urlobj.openConnection();
			postConnection.setRequestMethod("POST");
			
			postConnection.setRequestProperty("name", "a1bcdefgh");
			postConnection.setRequestProperty("Content-Type", "application/json");

			postConnection.setDoOutput(true);
			OutputStream os = postConnection.getOutputStream();
			BufferedWriter writer = new BufferedWriter(
			        new OutputStreamWriter(os, "UTF-8"));
			writer.write(getQuery(list));
			writer.flush();
			writer.close();
			os.close();
			
			postConnection.connect();
			int  responseCode= postConnection.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_CREATED)
			{
		    	BufferedReader in  = new BufferedReader(new InputStreamReader(postConnection.getInputStream()));
				String input;
				StringBuffer response = new StringBuffer();
				while((input = in.readLine())!=null)
				{
					response.append(input);
					
				}in.close();
				System.out.println(response.toString());
				
			}
			else
			{
				System.out.println("POST NOT WORKED");
			}

		}
		private static String getQuery(List<FileDetails> list) throws UnsupportedEncodingException
		{
		    StringBuilder result = new StringBuilder();
		    boolean first = true;

		    for (FileDetails pair : list)
		    {
		        if (first)
		            first = false;
		        else
		            result.append("&");

		        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
		        result.append("=");
		        result.append(URLEncoder.encode(pair.getJob(), "UTF-8"));
		    }

		    return result.toString();
		}
		
		public static void enableForClass(Class<ParallelInterfaceCall> className) {
	        systemOutToLogger = new FileReading(originalSystemOut, className.getName());
	        System.setOut(systemOutToLogger);
	    }

		



}
