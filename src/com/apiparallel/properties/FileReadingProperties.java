package com.apiparallel.properties;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
public class FileReadingProperties {

	public void propertiesFileRead()
	{
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input= new FileInputStream("C:\\Users\\jayal\\eclipse_project_workspace\\APIParallelProject\\Resources\\config.properties");


		    // load a properties file
		    prop.load(input);

		    // get the property value and print it out
		    System.out.println(prop.getProperty("thread.threadnumber"));
		    System.out.println(prop.getProperty("api.url"));
		    System.out.println(prop.getProperty("api.method"));

		} catch (IOException ex) {
		    ex.printStackTrace();
		} finally {
		    if (input != null) {
		        try {
		            input.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		    }
		}
	    }
}
