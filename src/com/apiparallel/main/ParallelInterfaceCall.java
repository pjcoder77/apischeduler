package com.apiparallel.main;
import java.io.IOException;
import java.util.logging.Logger;


import com.apiparallel.filedetails.FileDetails;
import com.apiparallel.fileoperation.FileReading;
import com.apiparallel.properties.FileReadingProperties;

public class ParallelInterfaceCall {

	final static Logger logger = Logger.getLogger(ParallelInterfaceCall.class.getName());
	static {
		FileReading.enableForClass(ParallelInterfaceCall.class);
    }
	
	public static void main(String[] args) throws IOException {
		FileReadingProperties pfr = new FileReadingProperties();
		pfr.propertiesFileRead();
		FileReading.fileRead();
		FileReading.apicall( FileReading.list);
		
		for(FileDetails i: FileReading.list)
		{
			System.out.println("name = "+ i.getName() + "\t"+ "job = "+ i.getJob());
			

		}
		
		logger.info("List of objects is created successfully!");		
	}

	

}
